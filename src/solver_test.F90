program solver_test
  use solver_mod 
  implicit none 

  integer, parameter :: nz = 100
  real(8), parameter :: PI = 3.14159265359d0
  real(8), dimension(nz+2,2) :: xo,xx,xinit 
  real(8), dimension(nz+2,3) :: zz 
  real(8) :: dt,fnorm,t,t0,err
  integer :: i,j
 
  ! Local test  
  xo = 0.d0 
  zz = 0.d0  
  do i=1+1,nz+1 
    zz(i,1) = DBLE(i-1) 
    zz(i,2) =-DBLE(i-1) 
  enddo   
  
  xx = 0.d0  
  dt = 1.d0 
  do i=1,50
    do j=1,10
      fnorm = solver_do_update(dt,xo,zz,xx,localNonlinearFunc,.false.)   
      if (fnorm<1.d-12) exit 
    enddo
    xo = xx  
  enddo 
    
  write(6,'(a,3es12.3)')'Local test: ',fnorm,SUM(zz(2:nz+1,1)**2*0.5d0-xx(2:nz+1,1)**2), &
      SUM(zz(2:nz+1,2)**2*0.5d0-xx(2:nz+1,2)**2)
  
  ! Diffusion 
  zz = 1.d0 ! Set zone size and diffusion coefficient to unity
  t0 = 5.d0  
  do i=2,nz+1
    xx(i,:) = EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t0))/SQRT(4.d0*PI*t0)  
  enddo  
  xo = xx
  xinit = xx  
  dt = 5.d-1 
  t = t0
  do i=1,50
    do j=1,10
      fnorm = solver_do_update(dt,xo,zz,xx,diffusionFunc,.false.)   
      if (fnorm<1.d-12) exit 
    enddo
    xo = xx 
    t = t + dt  
  enddo

  err = 0.d0  
  do i=2,nz+1
    !print *,DBLE(i-1),xinit(i,1),xx(i,1),EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t))/SQRT(4.d0*PI*t)
    err = err + ABS(xx(i,1) - EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t))/SQRT(4.d0*PI*t))
  enddo    
  
  write(6,'(a,3es12.3)')'Diffusion test 1: ',fnorm,err
  
  ! Diffusion two func
  zz = 1.d0 ! Set zone size and diffusion coefficient to unity
  t0 = 5.d0 
  xx = 0.d0  
  do i=2,nz+1
    xx(i,1) = EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t0))/SQRT(4.d0*PI*t0) 
    xx(i-1,2) = -(xx(i,1) - xx(i-1,1)) 
  enddo 
  xx(nz+1,2) = xx(nz+1,1)
    
  xo = xx
  xinit = xx  
  dt = 5.d-1 
  t = t0
  do i=1,50
    do j=1,10
      fnorm = solver_do_update(dt,xo,zz,xx,diffusionTwoFuncs,.false.)   
      if (fnorm<1.d-12) exit 
    enddo
    if (fnorm>1.d-12) print *,fnorm
    xo = xx 
    t = t + dt  
  enddo

  err = 0.d0  
  do i=2,nz+1
    !print *,DBLE(i-1),xinit(i,1),xx(i,1),EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t))/SQRT(4.d0*PI*t)
    err = err + ABS(xx(i,1) - EXP(-(DBLE(i-1)-50.d0)**2/(4.d0*t))/SQRT(4.d0*PI*t))
  enddo    
  
  write(6,'(a,3es12.3)')'Diffusion test 2: ',fnorm,err
    
contains 

  function localNonlinearFunc(xl,xm,xu,xlo,xmo,xuo,zl,zm,zu,inner_bound,outer_bound,output_call) result(y) 
    implicit none 
    real(8), dimension(:), intent(in) :: xl
    real(8), dimension(SIZE(xl)), intent(in) :: xm,xu ! Independent variables
    real(8), dimension(SIZE(xl)), intent(in) :: xlo,xmo,xuo ! Independent variables at old time
    real(8), dimension(:), intent(in) :: zl,zm,zu ! Fixed variables
    logical, intent(in) :: inner_bound,outer_bound,output_call 
    real(8), dimension(SIZE(xl)) :: y 
    y = 0.d0 
    y(1) = xm(1) - xmo(1) - dt*(zm(1)**2 - xm(1)**2 - xm(2)**2) 
    y(2) = xm(2) - xmo(2) - dt*(xm(1)**2 - xm(2)**2) 
    if (output_call) write(6,'(i4,20es12.3)')SIZE(xm),y,xm,zm
    return  
  end function localNonlinearFunc
  
  function diffusionFunc(xl,xm,xu,xlo,xmo,xuo,zl,zm,zu,inner_bound,outer_bound,output_call) result(y) 
    implicit none 
    real(8), dimension(:), intent(in) :: xl
    real(8), dimension(SIZE(xl)), intent(in) :: xm,xu ! Independent variables
    real(8), dimension(SIZE(xl)), intent(in) :: xlo,xmo,xuo ! Independent variables at old time
    real(8), dimension(:), intent(in) :: zl,zm,zu ! Fixed variables
    logical, intent(in) :: inner_bound,outer_bound,output_call 
    real(8), dimension(SIZE(xl)) :: y 
    
    y = 0.d0 
    if (inner_bound) then 
      y(:) = xm(:) - xmo(:) - dt*zm(2)/zm(1)**2*(xu(:) - 2.d0*xm(:)) 
    elseif (outer_bound) then 
      y(:) = xm(:) - xmo(:) - dt*zm(2)/zm(1)**2*(xl(:) - 2.d0*xm(:)) 
    else 
      y(:) = xm(:) - xmo(:) - dt*zm(2)/zm(1)**2*(xl(:) + xu(:) - 2.d0*xm(:)) 
    endif  
    
    if (output_call) write(6,'(i4,20es12.3)')SIZE(xm),y,xm,zm
    
    return  
  end function diffusionFunc
  
  function diffusionTwoFuncs(xl,xm,xu,xlo,xmo,xuo,zl,zm,zu,inner_bound,outer_bound,output_call) result(y) 
    implicit none 
    real(8), dimension(:), intent(in) :: xl
    real(8), dimension(SIZE(xl)), intent(in) :: xm,xu ! Independent variables
    real(8), dimension(SIZE(xl)), intent(in) :: xlo,xmo,xuo ! Independent variables at old time
    real(8), dimension(:), intent(in) :: zl,zm,zu ! Fixed variables
    logical, intent(in) :: inner_bound,outer_bound,output_call 
    real(8), dimension(SIZE(xl)) :: y 
    
    y = 0.d0 
    if (inner_bound) then 
      y(1) = xm(1) - xmo(1) + dt/zm(1)*(xm(2) + xm(1)/zm(1))  
      y(2) = xm(2) + zm(2)/zm(1)*(xu(1) - xm(1))
    elseif (outer_bound) then 
      y(1) = xm(1) - xmo(1) + dt/zm(1)*(xm(2)-xl(2))  
      y(2) = xm(2) - zm(2)/zm(1)*xm(1) 
    else 
      y(1) = xm(1) - xmo(1) + dt/zm(1)*(xm(2)-xl(2))
      y(2) = xm(2) + zm(2)/zm(1)*(xu(1) - xm(1))
    endif  
    
    if (output_call) write(6,'(i4,20es12.3)')SIZE(xm),y,xm,zm
    
    return  
  end function diffusionTwoFuncs
  
end program solver_test
